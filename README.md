
![](https://gitlab.com/ddokk/gfx/-/raw/main/g/b1.png)

---

<h1 align="center"><code> Books Mentioned </code></h1>
<h2 align="center"><i> Books Mentioned in the followingt talk - https://youtu.be/9v04zt_E378 </i></h2>

----
1. [Youtube Video](#youtube-video)
2. [Repo Description](#repo-description)
3. [Books Mentioned](#books-mentioned)

----

# Youtube Video 

[![](./t/y.png)](https://youtu.be/9v04zt_E378)

# Repo Description 

This repo are the information regarding the books mentioned in the above talk 

# Books Mentioned

N | Book | Img 
|:--:|:--:|:--:|
1 | [Qut al-Qulub - 2 vol <br> By: Makki, Abu Talib( قوت القلوبأبو طالب المكي )](https://alkitab.com/15352.html) | [![](https://s.turbifycdn.com/aah/yhst-141393827066280/qut-al-qulub-2-vol-1.jpg)](https://alkitab.com/15352.html)
2 | [Iḥyāʾ ʿulūm al-dīn <br> By: Abu Hamid Al-Ghazali](https://www.ghazali.org/rrs-ovr/) | [![](https://m.media-amazon.com/images/I/51S4tMrNV3L._SX330_BO1,204,203,200_.jpg)](https://www.ghazali.org/rrs-ovr/)
3 | [Madārij As Salikīn [English] Vol 1 and 2 <br> By:  Ibn al-Qayyim](https://www.kalamullah.com/madarij-al-salikin.html) | [![](https://adviceforparadise.com/media/book-pic/Madarij_as-Salikeen_Vol._1.png)](https://www.kalamullah.com/madarij-al-salikin.html)


